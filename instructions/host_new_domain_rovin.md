# Host new domain in 52.6.72.82 apache+uwsgi+django
good article in russian language: https://habr.com/post/226419/

## setup django
```bash
# connect
ssh root@52.6.72.82 -i ~/.ssh/cancel

# move to projects root
sudo su - django
cd /home/django/projects/
git clone

# set virtualenv
mkvirtualenv project_name
setvirtualenvproject
workon project_name

# create uwsgi.ini
[uwsgi]
socket = :8016  # it should be free socket
module = wsgi
chmod-socket = 666
uid = www-data
gid = www-data
touch-reload = /home/django/projects/newagecancel/app/main/wsgi.py  # full path to wsgi in your project
buffer-size= 32768
```

### try to run with wsgi
```bash
pip install uwsgi
python manage.py runserver 0.0.0.0:8071   # check 52.6.72.82:8071, should be ok
uwsgi --http :8071 --module mysite.wsgi   # check 52.6.72.82:8071, should be ok
```

## setup nginx
```bash
nano /etc/nginx/sites-enabled/__your_domain__.conf
nano /etc/nginx/sites-available/__your_domain__.conf
```

```
upstream django {
    # server unix:///path/to/your/mysite/mysite.sock; # unix socket, maybe later
    server 127.0.0.1:8015; # web-port
}

server {
    listen      8076;  # ! same free port as in apache
    server_name     newagecancel.com;
    charset     utf-8;

    client_max_body_size 75M;

    location /media  {
        alias /home/django/projects/__yor_domain__/static;
    }

    location /static {
        alias /home/django/projects/__yor_domain__/static;

    }

    location / {
        uwsgi_pass  django;
        include     uwsgi_params;
    }
        access_log /var/log/nginx/__yor_domain__-access.log;
        error_log /var/log/nginx/__yor_domain__.com-error.log;
}
```

### try static
```bash
# reload nginx
# in settings.py we have STATIC_ROOT = os.path.join(BASE_DIR, "static/")
python manage.py collectstatic
sudo /etc/init.d/nginx reload
yourserver.com:8076/media/media.png   # ! same port as in nginx and apache
```

## setup apache
```bash
nano /etc/apache2/sites-available/__your_domain__.conf
```
```
<VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        ServerName your_domain.com
        ServerAlias www.your_domain.com

        ServerAdmin webmaster@localhost

        ProxyPass / http://localhost:8076/
        ProxyPassReverse / http://localhost:8076/


        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/your_domain.error.log
</VirtualHost>
```

## run uwsgi
```bash
sudo /etc/init.d/nginx apache

uwsgi --ini=uwsgi.ini  # try to run yourdomain.com, should be ok.
# stop process and run same as a daemon
uwsgi --ini=uwsgi.ini --daemonize=/home/django/projects/newcancel/logs/uwsgi.log

#then for reload python code you can
touch path_to/wsgi.py  # full path to wsgi in your project

```