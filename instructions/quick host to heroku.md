http://cookiecutter-django.readthedocs.org/en/latest/deployment-on-heroku.html
```bash
LOCAL
#1) Install django cutter
mkdir project_name
cd project_name
cookiecutter --no-input https://github.com/arhangel66/cookiecutter-django project_name=test2
cookiecutter https://github.com/pydanny/cookiecutter-django project_name=ezcorp

#2) Make git branch:
curl --user arhangel66:arhderb1985 https://api.bitbucket.org/1.0/repositories/ --data name=ezcorp

#3) Git push all:
cd post_tool
git init
git remote add origin https://arhangel66@bitbucket.org/arhangel66/quick-heroku.git
git add .
git commit -m 'init'
git push -u origin master

#4) pip install virtualenv
virtualenv ../venv
. ../venv/scripts/activate

#5) pip install -r requirements/local.txt

#6) pip install django-debug-toolbar
pip install django-extensions==1.6.1

#7)
createdb -h localhost -U postgres -p 5432 q_heroku
add to local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'quick-heroku',
        'USER': 'postgres',
        'PASSWORD': 'postgrespass',
    }
}
#8)
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver

#9)
users/adapter.py
is_open_for_signup(self, request, social=None)
```

###HEROKU:
```bash
heroku create --buildpack https://github.com/heroku/heroku-buildpack-python
heroku apps:rename post_tool
heroku addons:create heroku-postgresql:hobby-dev
heroku pg:backups schedule --at '02:00 America/Los_Angeles' DATABASE_URL

heroku config:set DJANGO_SECRET_KEY=`openssl rand -base64 64`
heroku config:set DJANGO_SETTINGS_MODULE='config.settings.production'
heroku config:set DJANGO_ALLOWED_HOSTS='.herokuapp.com'
heroku config:set DJANGO_AWS_ACCESS_KEY_ID='asdf'
heroku config:set AWS_SECRET_ACCESS_KEY='asdf'
heroku config:set AWS_STORAGE_BUCKET_NAME='asdf'
heroku config:set MAILGUN_API_KEY='key-3a38490e42d9cd13255975ea8ab3cdcc'
heroku config:set MAILGUN_SENDER_DOMAIN='sandboxec63490ccf724ef887039d9a4b0e0cc7.mailgun.org'
heroku pg:promote DATABASE_URL
heroku config:set PYTHONHASHSEED=random
heroku config:set DJANGO_ADMIN_URL=\^admin/
heroku config:set DJANGO_DEBUG=True
git push heroku master  ||  git push heroku yourbranch:master

Если проблемы со статик файлами - убедись что берёшь их через whitenoise и все env в production.py закрыты

heroku run python manage.py migrate
heroku run python manage.py check --deploy
heroku run python manage.py createsuperuser
heroku open
```

#for login with socialapp:
```
- create app here:
/admin/socialaccount/socialapp/

facebook:
https://developers.facebook.com/

google:
https://console.developers.google.com/flows/enableapi?apiid=analytics&credential=client_key
```


heroku config:set DJANGO_AWS_ACCESS_KEY_ID=AKIAJO537LKRVPHAYBJA
heroku config:set DJANGO_AWS_SECRET_ACCESS_KEY=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



git remote add heroku git@heroku.com:sandtex.git
git config heroku.remote heroku


docker-machine create \
-d digitalocean \
--digitalocean-access-token=f3716b96f45a656b3327844c2dfddc086ad3cfb16cb23cdfeb1c5cf0bc57c394 \
dae-prod
eval $(docker-machine env dae-prod)
docker-compose -f production.yml build
docker-compose -f production.yml up -d

docker-compose -f production.yml build && docker-compose -f production.yml up -d

docker-machine rm dae-prod



docker-machine create --driver virtualbox dae
eval $(docker-machine env dev)
docker-machine ip dae
docker-machine ssh dae
sudo su
echo 'ln -sfn /mnt/sda1/data /data' >> /var/lib/boot2docker/bootlocal.sh

docker-compose -f local.yml build
docker-compose -f local.yml up -d


my token: cfbc1e0f113dc772b7592ec49cf4fecf833cd4e363256eb409bac9788396ade3

docker-compose -f local.yml run django python manage.py makemigrations
docker-compose -f local.yml run django python manage.py migrate
docker-compose -f local.yml run django python manage.py collectstatic
docker-compose -f local.yml run django python manage.py createsuperuser





docker-machine create -d digitalocean  \
      --digitalocean-access-token f3716b96f45a656b3327844c2dfddc086ad3cfb16cb23cdfeb1c5cf0bc57c394 \
      --digitalocean-image ubuntu-16-04-x64  test-machine3



mkvirtualenv -p /Library/Frameworks/Python.framework/Versions/3.6/bin/python3 ezcorp -  will create with python3.6 
setvirtualenvproject - will set current dir as projectΩ root

##SUPER_QUICK(!)
```bash
cd ~/w/
mkdir ezcorp
cd ezcorp
#cookiecutter --no-input https://github.com/arhangel66/cookiecutter-django project_name=ezcorp
cookiecutter
curl --user arhangel66:arhderb1985 https://api.bitbucket.org/1.0/repositories/ --data name=schumplaner_mirror2
cd ezcorp
git init
git remote add origin https://arhangel66@bitbucket.org/arhangel66/base.git
git add .
git commit -m 'init'
git push -u origin master
mkvirtualenv -p /Library/Frameworks/Python.framework/Versions/3.6/bin/python3 ezcorp 
setvirtualenvproject
pip install -r requirements/local.txt

createdb -h localhost -U postgres -p 5432 ezcorp
echo 'DATABASE_URL=postgres://postgres:postgrespass@127.0.0.1:5432/ezcorp2' >.env
python manage.py migrate
echo "from django.contrib.auth import get_user_model;User = get_user_model();User.objects.filter(email='arhangel662@gmail.com').delete();User.objects.create_superuser('arhangel662', 'arhangel662@gmail.com', 'arhderb1985')" | python manage.py shell

heroku create --buildpack https://github.com/heroku/heroku-buildpack-python
heroku apps:rename ezcorp
heroku addons:create heroku-postgresql:hobby-dev
heroku pg:backups schedule --at '02:00 America/Los_Angeles' DATABASE_URL
heroku pg:promote DATABASE_URL

heroku addons:create heroku-redis:hobby-dev
heroku addons:create mailgun

heroku config:set DJANGO_ADMIN_URL="$(openssl rand -base64 32)"
heroku config:set DJANGO_SECRET_KEY="$(openssl rand -base64 64)"
heroku config:set DJANGO_SETTINGS_MODULE='config.settings.production'
heroku config:set DJANGO_ALLOWED_HOSTS='.herokuapp.com'

heroku config:set DJANGO_AWS_ACCESS_KEY_ID='a'
heroku config:set DJANGO_AWS_SECRET_ACCESS_KEY='a'
heroku config:set DJANGO_AWS_STORAGE_BUCKET_NAME='a'

heroku config:set DJANGO_MAILGUN_SERVER_NAME=''
heroku config:set MAILGUN_API_KEY='key-3a38490e42d9cd13255975ea8ab3cdcc'
heroku config:set MAILGUN_SENDER_DOMAIN='sandboxec63490ccf724ef887039d9a4b0e0cc7.mailgun.org'
heroku config:set DJANGO_SENTRY_DSN=''

heroku config:set PYTHONHASHSEED=random
heroku config:set DJANGO_ADMIN_URL=\^admin/
git push heroku master
heroku run python manage.py migrate
heroku run python manage.py check --deploy
heroku run '"from django.contrib.auth import get_user_model;User = get_user_model();User.objects.filter(email='arhangel662@gmail.com').delete();User.objects.create_superuser('arhangel662', 'arhangel662@gmail.com', 'arhderb1985')" | python manage.py shell'
heroku run '"from django.contrib.auth import get_user_model;User = get_user_model();User.objects.filter(email='admin@admin.com').delete();User.objects.create_superuser('admin', 'admin@admin.com', 'admpass')" | python manage.py shell'
heroku open
```


git push --mirror https://bitbucket.org/arhangel66/schumplaner_mirror.git