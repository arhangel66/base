```bash
sudo nano /etc/hosts
```

and then:
```bash
##
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting. Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1 localhost
fe80::1%lo0	localhost
```

127.0.0.1 wap.megafonpro.ru
127.0.0.1 podpiskipro.ru
127.0.0.1 www.podpiskipro.ru
127.0.0.1 megafonpro.ru
127.0.0.1 www.megafonpro.ru
