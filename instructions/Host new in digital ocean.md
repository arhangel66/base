```
ssh -i ~/.ssh/id_rsa root@138.197.186.28
cd /home/webapp/ll_api/src
docker-compose -f local.yml run django python manage.py migrate
```

```
mkdir /home/webapp/
cd /home/webapp/ && mkdir .git-m & cd .git-m && git init --bare
chmod ug+x /home/webapp/.git-m/hooks/*
nano /home/webapp/.git-m/hooks/post-receive
```

put to post-receive

```
!/bin/sh
git --work-tree=/home/webapp/ll_api/src --git-dir=/home/webapp/.git-m checkout -f
```

```bash
mkdir /home/webapp/ll_api/
mkdir /home/webapp/ll_api/src
cd /home/webapp/ll_api/src
```

**In local pc:**

```sh
nano ~/.ssh/config
# put:
Host ll-api-git
  HostName 138.197.186.28
  User root
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa
```

For project add git remote in pycharm like that:

```
ssh://ll-api-git/home/webapp/.git-m
```

![image](https://monosnap.com/direct/QBQvzdb0tZetY7ZKtvYmRi2dFuqmea)

Then we can run in local:

```
git push my-server master
```

It will automatically copy new files to src folder in server and rerun docker

**Run docker**

```
docker-compose -f local.yml build --force-rm
docker-compose -f local.yml up -d django
docker-compose -f local.yml run django python manage.py migrate
```

Production
==========

[http://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html\#envs](http://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html#envs)

<http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html>