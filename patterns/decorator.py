
from abc import ABC, abstractmethod


class Hero:
    def __init__(self):
        self.positive_effects = []
        self.negative_effects = []

        self.stats = {
            "HP": 128,
            "MP": 42,
            "SP": 100,

            "Strength": 15,
            "Perception": 4,
            "Endurance": 8,
            "Charisma": 2,
            "Intelligence": 3,
            "Agility": 8,
            "Luck": 1
        }

    def get_positive_effects(self):
        return self.positive_effects.copy()

    def get_negative_effects(self):
        return self.negative_effects.copy()

    def get_stats(self):
        return self.stats.copy()


class AbstractEffect(Hero, ABC):
    def __init__(self, base):
        self.base = base

    @abstractmethod
    def get_stats(self):
        return self.base.get_stats()

    def get_positive_effects(self):
        return self.base.get_positive_effects()

    def get_negative_effects(self):
        return self.base.get_negative_effects()


class AbstractPositive(AbstractEffect, ABC):
    def get_positive_effects(self):
        return self.base.get_positive_effects() + [type(self).__name__]


class AbstractNegative(AbstractEffect, ABC):
    def get_negative_effects(self):
        return self.base.get_negative_effects() + [type(self).__name__]


class Berserk(AbstractPositive):
    def get_stats(self):
        stats = self.base.get_stats()
        stats["HP"] += 50
        stats["Strength"] += 7
        stats["Endurance"] += 7
        stats["Agility"] += 7
        stats["Luck"] += 7
        stats["Perception"] -= 3
        stats["Charisma"] -= 3
        stats["Intelligence"] -= 3
        return stats


class Blessing(AbstractPositive):
    def get_stats(self):
        stats = self.base.get_stats()
        stats["Strength"] += 2
        stats["Endurance"] += 2
        stats["Agility"] += 2
        stats["Luck"] += 2
        stats["Perception"] += 2
        stats["Charisma"] += 2
        stats["Intelligence"] += 2
        return stats


class Weakness(AbstractNegative):
    def get_stats(self):
        stats = self.base.get_stats()
        stats["Strength"] -= 4
        stats["Endurance"] -= 4
        stats["Agility"] -= 4
        return stats


class EvilEye(AbstractNegative):
    def get_stats(self):
        stats = self.base.get_stats()
        stats["Luck"] -= 10
        return stats


class Curse(AbstractNegative):
    def get_stats(self):
        stats = self.base.get_stats()
        stats["Strength"] -= 2
        stats["Endurance"] -= 2
        stats["Agility"] -= 2
        stats["Luck"] -= 2
        stats["Perception"] -= 2
        stats["Charisma"] -= 2
        stats["Intelligence"] -= 2
        return stats

# class AbstractEffect(Hero, ABC):
#     params = {}
#
#     def __init__(self, base):
#         self.base = base
#
#     @abstractmethod
#     def get_effect(self):
#         return self.params
#
#     def get_stats(self):  # Возвращает итоговые хараетеристики
#         # после применения эффекта
#         stats = self.base.get_stats()
#         params = self.get_effect()
#         stats = {k: params.get(k, 0) + stats.get(k, 0) for k in set(stats)}
#         return stats
#
#     def get_positive_effects(self):
#         self.base.get_positive_effects()
#
#     def get_negative_effects(self):
#         self.base.get_positive_effects()
#
#
# class AbstractPositive(AbstractEffect):
#     def get_positive_effects(self):
#         return self.base.get_positive_effects() + [type(self).__name__]
#
#
# class AbstractNegative(AbstractEffect):
#     def get_negative_effects(self):
#         return self.base.get_negative_effects() + [type(self).__name__]
#
#
# class Berserk(AbstractPositive):
#     """
#     Увеличивает параметры Сила, Выносливость, Ловкость, Удача на 7; уменьшает параметры Восприятие,
#     Харизма, Интеллект на 3. Количество единиц здоровья увеличивается на 50.
#     """
#
#     def get_effect(self):
#         return {
#             "HP": 50,
#             "Strength": 7,
#             "Endurance": 7,
#             "Luck": 7,
#             "Agility": 7,
#             "Perception": -3,
#             "Charisma": -3,
#             "Intelligence": -3,
#         }
#
#
# class Blessing(AbstractPositive):
#     def get_effect(self):
#         return {
#             "Strength": 2,
#             "Endurance": 2,
#             "Luck": 2,
#             "Agility": 2,
#             "Perception": 2,
#             "Charisma": 2,
#             "Intelligence": 2
#         }
#
#
# class Weakness(AbstractNegative):
#     def get_effect(self):
#         return {
#             "Strength": -4,
#             "Endurance": -4,
#             "Agility": -4,
#         }
#
#
# class EvilEye(AbstractNegative):
#     def get_effect(self):
#         return {
#             'Luck': -10
#         }
#
#
# class Curse(AbstractNegative):
#     def get_effect(self):
#         return {
#             "Strength": -2,
#             "Endurance": -2,
#             "Luck": -2,
#             "Agility": -2,
#             "Perception": -2,
#             "Charisma": -2,
#             "Intelligence": -2
#         }
# Благословение — Увеличивает все основные характеристики на 2.
# Слабость — Уменьшает параметры Сила, Выносливость, Ловкость на 4.
# Сглаз — Уменьшает параметр Удача на 10.
# Проклятье — Уменьшает все основные характеристики на 2.
