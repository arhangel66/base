class System:
    def __init__(self):
        self.map = self.grid = [[0 for i in range(30)] for _ in range(20)]
        self.map[5][7] = 1  # Источники света
        self.map[5][2] = -1  # Стены

    def get_lightening(self, light_mapper):
        self.lightmap = light_mapper.lighten(self.map)


class Light:
    def __init__(self, dim):
        self.dim = dim
        self.grid = [[0 for i in range(dim[0])] for _ in range(dim[1])]
        self.lights = []
        self.obstacles = []

    def set_dim(self, dim):
        self.dim = dim
        self.grid = [[0 for i in range(dim[0])] for _ in range(dim[1])]

    def set_lights(self, lights):
        self.lights = lights
        self.generate_lights()

    def set_obstacles(self, obstacles):
        self.obstacles = obstacles
        self.generate_lights()

    def generate_lights(self):
        return self.grid.copy()


class MappingAdapter:
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def lighten(self, grid):
        self.adaptee.set_dim((len(grid[1]), len(grid)))
        # get lights from grid
        lights = self.get_lights(grid)
        self.adaptee.set_lights(lights)

        obstacles = self.get_obstacles(grid)
        self.adaptee.set_obstacles(obstacles)

        return self.adaptee.generate_lights()

    def get_lights(self, grid):
        return self._get_object(grid, 1)

    def get_obstacles(self, grid):
        return self._get_object(grid, -1)

    def _get_object(self, grid, code):
        res = []
        for i, line in enumerate(grid):
            print(i)
            while code in line:
                res.append((line.index(code), i))
                line[line.index(code)] = 0
        return res
