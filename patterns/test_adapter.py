import logging
from logging import getLogger, DEBUG

from .adapter import System, MappingAdapter, Light

logger = getLogger(__name__)
logger.setLevel(DEBUG)
logger.addHandler(logging.StreamHandler())


class TestAdapter:
    def test_run(self):
        system = System()
        light = Light([3, 2])  # maybe here I have to set not object, just class
        light_mapper = MappingAdapter(light)
        system.get_lightening(light_mapper)
        print(17, system.lightmap)
        assert isinstance(system.lightmap, list)

    def test_get_lights(self):
        map = [[0 for i in range(30)] for _ in range(20)]
        logger.debug(map)
        map[2][3] = 1
        map[2][7] = 1
        map[1][8] = 1
        light_mapper = MappingAdapter(None)
        assert light_mapper.get_lights(map) == [(8, 1), (3, 2), (7, 2)]

    def test_get_obstacles(self):
        map = [[0 for i in range(30)] for _ in range(20)]
        map[2][3] = -1
        map[1][8] = -1
        light_mapper = MappingAdapter(None)
        assert light_mapper.get_obstacles(map) == [(8, 1), (3, 2)]
