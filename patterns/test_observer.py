from .observer import ShortNotificationPrinter, FullNotificationPrinter, ObservableEngine

class TestObserver:
    @classmethod
    def setup_class(cls):
        cls.messages = [
            {"title": "Покоритель", "text": "Дается при выполнении всех заданий в игре"},
            {"title": "Покоритель", "text": "Дается при выполнении всех заданий в игре"},
            {"title": "новый покоритель", "text": "новый"}
        ]

    def test_update_short(self):
        short = ShortNotificationPrinter()
        for message in self.messages:
            short.update(message)

        assert len(short.achievements) == 2
        assert short.achievements == {'Покоритель', 'новый покоритель'}


    def test_update_full(self):
        full = FullNotificationPrinter()
        for message in self.messages:
            full.update(message)

        assert len(full.achievements) == 2
        assert full.achievements == [
            {"title": "Покоритель", "text": "Дается при выполнении всех заданий в игре"},
            {"title": "новый покоритель", "text": "новый"}
        ]

    def test_engine_subscribe(self):
        engine = ObservableEngine()
        subscriber = FullNotificationPrinter()
        engine.subscribe(subscriber)
        print(dir(engine))
        assert engine._ObservableEngine__subscribers == {subscriber}


    def test_engine_unsubscribe(self):
        engine = ObservableEngine()
        subscriber = FullNotificationPrinter()
        engine._ObservableEngine__subscribers = {subscriber}
        assert engine._ObservableEngine__subscribers == {subscriber}

        engine.unsubcribe(subscriber)
        assert engine._ObservableEngine__subscribers == set()



