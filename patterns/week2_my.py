import math

SCREEN_DIM = (800, 600)
class Vec2d:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __getitem__(self, index):
        l = [self.x, self.y]
        return l[index]

    def __add__(self, other):
        # self.x += other.x
        # self.y += other.y
        return Vec2d(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __mul__(self, mul):
        # self.x *= mul
        # self.y *= mul
        return Vec2d(self.x * mul, self.y * mul)

    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def int_pair(self):
        return (int(self.x), int(self.y))

    def __str__(self):
        return "x=%s, y=-%s" % (self.x, self.y)

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        return str((self.x, self.y))

    def __iter__(self):
        for el in [self.x, self.y]:
            yield el


class Polyline:
    def __init__(self, points):
        self.points = []
        for point in points:
            self.points.append(Vec2d(point[0], point[1]))
        print(43, self.points)

    def __add__(self, point):
        pass

    # Персчитывание координат опорных точек
    def set_points_old(self, points, speeds):
        for p in range(len(points)):
            points[p] = add(points[p], speeds[p])
            if points[p][0] > SCREEN_DIM[0] or points[p][0] < 0:
                speeds[p] = (- speeds[p][0], speeds[p][1])
            if points[p][1] > SCREEN_DIM[1] or points[p][1] < 0:
                speeds[p] = (speeds[p][0], -speeds[p][1])

    def set_points(self, speeds):
        points = self.points
        for p in range(len(points)):
            points[p] = points[p] + speeds[p]
            if points[p].x > SCREEN_DIM[0] or points[p][0] < 0:
                speeds[p] = (- speeds[p][0], speeds[p][1])
            if points[p][1] > SCREEN_DIM[1] or points[p][1] < 0:
                speeds[p] = (speeds[p][0], -speeds[p][1])

    def draw_points(self):
        pass


class Knot(Polyline):
    def get_point(self, points, alpha, deg=None):
        if deg is None:
            deg = len(points) - 1
        if deg == 0:
            return points[0]
        return points[deg] * alpha + self.get_point(points, alpha, deg - 1) * (1 - alpha)


    def get_points(self, base_points, count):
        alpha = 1 / count
        res = []
        for i in range(count):
            res.append(self.get_point(base_points, i * alpha))
        return res

    def get_knot(self, count):
        points = self.points

        if len(points) < 3:
            return []
        res = []
        for i in range(-2, len(points) - 2):
            ptn = []
            ptn.append((points[i] + points[i + 1]) * 0.5)
            ptn.append(points[i + 1])
            ptn.append((points[i + 1] + points[i + 2]) * 0.5)
            # print(90, ptn, i, points)

            res.extend(self.get_points(ptn, count))
        return res


v1 = Vec2d(1, 1)
v2 = Vec2d(2, 2)
print(v1 + v2)

points = [(0, 0), (0, 100), (100, 0)]
# assert get_knot(points, 2) == 1
