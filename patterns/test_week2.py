from .week2 import get_knot, add, sub, mul
from .week2_my import Vec2d, Knot


class TestVec2d:
    def test_add(self):
        v1 = Vec2d(1, 1)
        print(v1)
        print(v1[0])
        print(v1[1])
        v2 = Vec2d(2, 2)
        assert (v1 + v2).x == add((1, 1), (2, 2))[0]



    def test_sub(self):
        v1 = Vec2d(1, 1)
        v2 = Vec2d(2, 2)
        assert (v2 - v1).x == sub((2, 2), (1, 1))[0]

    def test_mul(self):
        v1 = Vec2d(2, 2)
        assert (v1 * 3).x == mul((2, 2), 3)[0]

    def test_len(self):
        v1 = Vec2d(3, 4)
        assert v1.length() == 5

    def test_int_pair(self):
        v1 = Vec2d(3, 4.4)
        assert v1.int_pair() == (3, 4)


class TestPolyline:
    @classmethod
    def setup_class(cls):
        cls.points = [(0, 0), (0, 100), (100, 0)]

    def test_set_points(self):
        speeds = [(random.random() * 2, random.random() * 2) for i in range(len(self.points))]
        set_points(cls.points, speeds)
        # assert

    def test_draw_points(self):
        assert False


class TestKnot:
    @classmethod
    def setup_class(cls):
        cls.points = [(0, 0), (0, 100), (100, 0)]

    def test_get_knot(self):
        assert str(get_knot(self.points, 2)) == str(Knot(self.points).get_knot(2))
