import pytest

from .decorator import Hero, Berserk, Curse, Blessing, Weakness, AbstractEffect, AbstractPositive


class TestDecorator:
    @classmethod
    def setup_class(cls):
        cls.hero = Hero()

    def test_hero(self):
        assert self.hero.get_stats() == {"HP": 128, "MP": 42, "SP": 100, "Strength": 15, "Perception": 4,
                                         "Endurance": 8,
                                         "Charisma": 2, "Intelligence": 3, "Agility": 8, "Luck": 1}
        assert self.hero.get_negative_effects() == []
        assert self.hero.get_positive_effects() == []

    def test_berserk(self):
        """
        Увеличивает параметры Сила, Выносливость, Ловкость, Удача на 7; уменьшает параметры Восприятие, Харизма,
        Интеллект на 3. Количество единиц здоровья увеличивается на 50.
        :return:
        """
        hero_berserk = Berserk(self.hero)

        assert hero_berserk.get_stats()['Strength'] == self.hero.get_stats()['Strength'] + 7
        assert hero_berserk.get_stats()['Intelligence'] == self.hero.get_stats()['Intelligence'] - 3
        assert hero_berserk.get_positive_effects() == ['Berserk']

    def test_blessing(self):
        hero_blessing = Blessing(self.hero)

        assert hero_blessing.get_stats()['Strength'] == self.hero.get_stats()['Strength'] + 2

    def test_weakness(self):
        hero_weakness = Weakness(self.hero)

        assert hero_weakness.get_stats()['Strength'] == self.hero.get_stats()['Strength'] - 4

    def test_curse(self):
        hero_curse = Curse(self.hero)
        assert hero_curse.get_stats()['Strength'] == self.hero.get_stats()['Strength'] - 2

    def test_several(self):
        hero = Weakness(Curse(self.hero))
        assert hero.get_stats()['Strength'] == self.hero.get_stats()['Strength'] - 6
        assert hero.get_negative_effects() == ['Curse', 'Weakness']

    def test_abstract(self):
        with pytest.raises(TypeError):
            hero = AbstractPositive(self.hero)

        with pytest.raises(TypeError):
            hero = AbstractEffect(self.hero)


"""
# Благословение — Увеличивает все основные характеристики на 2.
# Слабость — Уменьшает параметры Сила, Выносливость, Ловкость на 4.
# Сглаз — Уменьшает параметр Удача на 10.
# Проклятье — Уменьшает все основные характеристики на 2.
"""
