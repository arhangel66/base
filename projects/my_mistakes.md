I realized that it's not a good idea to **change related_name** at a later point in development because then all usages 
of  Customer.child_set (for that specific example) will break.
