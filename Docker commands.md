```sh
docker-compose -f local.yml build --force-rm # build
docker-compose -f local.yml up django #up
docker ps
docker images
docker stop ...
docker container ls  # show all containters

# update celery
docker-compose -f local.yml restart celery-worker

docker-compose -f local.yml run --rm django python manage.py collectstatic
docker-compose -f local.yml run --rm django python manage.py createsuperuser
docker-compose -f local.yml run --rm django pip install google-measurement-protocol


# docker ssh
docker exec -it mongo_admin_local_django  bash 

# close all
docker stop $(docker ps -q)
```

