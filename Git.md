```bash
git submodule init # инициализировать его подмодуль
git submodule update fill_forms_irs # аналог git pull для подмодулей

git clone git@gitlab.schulmenueplaner.de:schulmenueplaner/schulmenueplaner.git mikhailssl

git remote -v
git fetch --all  # подгрузить все ромутные ветки
git origin show remote # показать все ветки на origin
git branch -a  # Показать все ветки


# как поменять origin
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:new-age-tech/ein/tranquil.git
git push -u origin --all
git push -u origin --tags
git fetch --all
git checkout master
git branch -u origin/master


# Удалить коммит (на самом деле не удаляется, а скрывается)
git reset --hard <sha1-commit-id>   # will set choosen commit as current... and 'delete' all next commits after it
git push origin HEAD --force




git reset --hard origin/152-mass-announcement-email-notice-pdf-template-function  # get from remote branch
например, https://stackoverflow.com/a/4474512/1557013




remove bad files:
https://rtyley.github.io/bfg-repo-cleaner/#examples

duplicate git to other:
https://help.github.com/articles/duplicating-a-repository/

git clone --bare https://gitlab.schulmenueplaner.de/schulmenueplaner/schulmenueplaner.git
cd schulmenueplaner.git
git push --mirror https://bitbucket.org/arhangel66/schumplaner_mirror2.git
cd ..
rm -rf schulmenueplaner.git


files:
java -jar bfg.jar --delete-files bossow_anon.sql schulmenueplaner.git
java -jar bfg.jar --delete-files schule_notes.md schulmenueplaner.git
java -jar bfg.jar --delete-files lastfailed schulmenueplaner.git
java -jar bfg.jar --delete-files cal 2019.zip schulmenueplaner.git
java -jar bfg.jar --delete-files celerybeat-schedule.db schulmenueplaner.git
java -jar bfg.jar --delete-files df_order.csv schulmenueplaner.git
java -jar bfg.jar --delete-files df_order2.csv schulmenueplaner.git
java -jar bfg.jar --delete-files do.py schulmenueplaner.git
java -jar bfg.jar --delete-files do.py.lprof schulmenueplaner.git
java -jar bfg.jar --delete-files ferien_baden-wuerttemberg_2019.ics schulmenueplaner.git
java -jar bfg.jar --delete-files menus_data.json schulmenueplaner.git
java -jar bfg.jar --delete-files ralbitz_reconcile_tax.py schulmenueplaner.git
java -jar bfg.jar --delete-files test.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test.xls schulmenueplaner.git
java -jar bfg.jar --delete-files test2.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test3.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files Untitled.ipynb schulmenueplaner.git


folders:
java -jar bfg.jar --delete-folders .cache schulmenueplaner.git
java -jar bfg.jar --delete-folders .pytest_cache schulmenueplaner.git
java -jar bfg.jar --delete-folders .ipynb_checkpoints schulmenueplaner.git
java -jar bfg.jar --delete-folders logs schulmenueplaner.git
java -jar bfg.jar --delete-folders 'zingst — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders vollwertkueche_old schulmenueplaner.git
java -jar bfg.jar --delete-folders sosprignitz_old schulmenueplaner.git
java -jar bfg.jar --delete-folders somic_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'rjo — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders remuss_old schulmenueplaner.git
java -jar bfg.jar --delete-folders mensakoechin_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'lfg — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders clauert_old schulmenueplaner.git
java -jar bfg.jar --delete-folders bantschow_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'Vorlage_Kartenlayout (1).html' schulmenueplaner.git
java -jar bfg.jar --delete-files bossow_anon.sql schulmenueplaner.git
java -jar bfg.jar --delete-files schule_notes.md schulmenueplaner.git
java -jar bfg.jar --delete-files lastfailed schulmenueplaner.git
java -jar bfg.jar --delete-files cal 2019.zip schulmenueplaner.git
java -jar bfg.jar --delete-files celerybeat-schedule.db schulmenueplaner.git
java -jar bfg.jar --delete-files df_order.csv schulmenueplaner.git
java -jar bfg.jar --delete-files df_order2.csv schulmenueplaner.git
java -jar bfg.jar --delete-files do.py schulmenueplaner.git
java -jar bfg.jar --delete-files do.py.lprof schulmenueplaner.git
java -jar bfg.jar --delete-files ferien_baden-wuerttemberg_2019.ics schulmenueplaner.git
java -jar bfg.jar --delete-files menus_data.json schulmenueplaner.git
java -jar bfg.jar --delete-files ralbitz_reconcile_tax.py schulmenueplaner.git
java -jar bfg.jar --delete-files test.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test.xls schulmenueplaner.git
java -jar bfg.jar --delete-files test2.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test3.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files Untitled.ipynb schulmenueplaner.git
java -jar bfg.jar --delete-files '010_греция_черногория.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files '012_лазерсон_кофе.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files '20171010_164952.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files 'Курсы_послушания.psd' schulmenueplaner.git
```

https://www.atlassian.com/git/tutorials/merging-vs-rebasing

```bash
git clone --mirror https://gitlab.schulmenueplaner.de/schulmenueplaner/schulmenueplaner.git

java -jar bfg.jar --delete-files test_reduction2.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files sample.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files test_reduction.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files test_reduction.csv  schulmenueplaner.git
java -jar bfg.jar --delete-files sample.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files mikhail_dump_20180611-1317.sql  schulmenueplaner.git
java -jar bfg.jar --delete-files lastfailed  schulmenueplaner.git

java -jar bfg.jar --delete-folders .cache schulmenueplaner.git
java -jar bfg.jar --delete-folders .pytest_cache schulmenueplaner.git
java -jar bfg.jar --delete-folders .ipynb_checkpoints schulmenueplaner.git
java -jar bfg.jar --delete-folders usercontent schulmenueplaner.git
java -jar bfg.jar --delete-folders logs schulmenueplaner.git

java -jar bfg.jar --delete-folders 'zingst — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders vollwertkueche_old schulmenueplaner.git
java -jar bfg.jar --delete-folders sosprignitz_old schulmenueplaner.git
java -jar bfg.jar --delete-folders somic_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'rjo — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders remuss_old schulmenueplaner.git
java -jar bfg.jar --delete-folders mensakoechin_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'lfg — копия' schulmenueplaner.git
java -jar bfg.jar --delete-folders clauert_old schulmenueplaner.git
java -jar bfg.jar --delete-folders bantschow_old schulmenueplaner.git
java -jar bfg.jar --delete-folders 'Vorlage_Kartenlayout (1).html' schulmenueplaner.git

java -jar bfg.jar --delete-files bossow_anon.sql schulmenueplaner.git
java -jar bfg.jar --delete-files schule_notes.md schulmenueplaner.git
java -jar bfg.jar --delete-files lastfailed schulmenueplaner.git
java -jar bfg.jar --delete-files 'cal 2019.zip' schulmenueplaner.git
java -jar bfg.jar --delete-files celerybeat-schedule.db schulmenueplaner.git
java -jar bfg.jar --delete-files df_order.csv schulmenueplaner.git
java -jar bfg.jar --delete-files df_order2.csv schulmenueplaner.git
java -jar bfg.jar --delete-files do.py schulmenueplaner.git
java -jar bfg.jar --delete-files do.py.lprof schulmenueplaner.git
java -jar bfg.jar --delete-files ferien_baden-wuerttemberg_2019.ics schulmenueplaner.git
java -jar bfg.jar --delete-files menus_data.json schulmenueplaner.git
java -jar bfg.jar --delete-files ralbitz_reconcile_tax.py schulmenueplaner.git
java -jar bfg.jar --delete-files test.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test.xls schulmenueplaner.git
java -jar bfg.jar --delete-files test2.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files test3.pdf schulmenueplaner.git
java -jar bfg.jar --delete-files Untitled.ipynb schulmenueplaner.git
java -jar bfg.jar --delete-files '010_греция_черногория.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files '012_лазерсон_кофе.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files '20171010_164952.jpg' schulmenueplaner.git
java -jar bfg.jar --delete-files 'Курсы_послушания.psd' schulmenueplaner.git

cd schulmenueplaner.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
git push
cd ../
rm -rf schulmenueplaner.git

java -jar bfg.jar --delete-files Mahnung_M100058.pdf  schulmenueplaner.git


~$test_reduction2.xlsx	Removed	kuechengeister
~$sample.xlsx	Removed	kuechengeister/kg/tests
test_reduction.xlsx	Removed	kuechengeister
test_reduction.csv	Removed	kuechengeister
sample.xlsx	Removed	kuechengeister/kg/tests
mikhail_dump_20180611-1317.sql	Removed
lastfailed	Removed	kuechengeister/.cache/v/cache


test_reduction2.xlsx
sample.xlsx
test_reduction.xlsx
test_reduction.csv
sample.xlsx
mikhail_dump_20180611-1317.sql
lastfailed



git clone --mirror https://gitlab.schulmenueplaner.de/schulmenueplaner/schulmenueplaner.git

java -jar bfg.jar --delete-files test_reduction2.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files sample.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files test_reduction.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files test_reduction.csv  schulmenueplaner.git
java -jar bfg.jar --delete-files sample.xlsx  schulmenueplaner.git
java -jar bfg.jar --delete-files mikhail_dump_20180611-1317.sql  schulmenueplaner.git
java -jar bfg.jar --delete-files lastfailed  schulmenueplaner.git
cd schulmenueplaner.git
git reflog expire --expire=now --all && git gc --prune=now --aggressive
git push
cd ../
rm -rf schulmenueplaner.git


```