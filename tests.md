unit test - quick
integration tests

One test - should always check only one method/view/etc
338


Things that should be tested:
- Views: viewing of data, changing of data and custom CBV methods
- models: creating/updating/deleting of mdoels, model methods, model managers
- forms: form methods, clean, methods, and custom fields
- validators: really dig and write multiple test methods against each custom validator you write.
- SIgnals - Since they act at distance, signals can cause grief especially if you lack test on them
- Filters - Since filters are essentially just functions accepting one or two arguments, writing tests for them should be easy
- template tags - Since template tags can do anything and can even accept template context, writing tests often becomes much more challenging. This means you really need to test them, since otherwise you may run into edge cases
- miscellany - context processors, middleware, email and anything else not covered in this list
- Failure - what happens when any of them above fail? Testing for system errors is as important as testing for system success.

p308


# pytest
```python
class TestDecorator:
    @classmethod
    def setup_class(cls):
        cls.hero = Hero()
```