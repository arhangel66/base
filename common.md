```
# waterflow
- Анализ требований
- Разработка Архитектуры
- Кодирование
- Тестирование и Отладка
- Инсталяция и поддержка
```

```html
# Парадигма проектирования
- Структурная парадигма (сверху вниз)
- Модульная
- Объектно-ориентированное
```

```html
Кульура программирования - читаемость
```

```html
качество:
функциональность:
- пригодность к использованию
- правильность выполнения задачи
- поддержка стандартов
- зищищенность
Надежность:
- редкость отказов
- отказоустойчивость
- способность к восстановлению

Практичность
- понятность
- управляемость
эффективность:
- время откилка
- ресурсы задействованы

Модульное тестирование (юнит) - тестирование отдельных операций, методов, функций
интеграционное тестирование -  проверка что модули взаимодействуют друг с другом корректно.
Системное тестирование - на уровне пользовательского интерфейса
regression testing - trying to find errors in ready code, when something become wrong.

Может ли программист быть тестировщиком? нет лучше разделять роли
```

# Контрактное программирование
- предусловия (что ты должен передать в функцию, если хочешь ее вызвать)
- постусловия (обязательства функции-поставщика, которые должны быть выполнены)
- инварианты (и то и другое?)
Все это записывается через assert
- жесткое падение проверяет выполнение контрактов
- проверка работает только в режиме отладки
PyContracts
```python
@contract(a='int,>0,<=100',
          b='list[N],N>0',
          returns='list[N]')
def my_function(a, b):
    pass
    
def my_function(a: 'int,>0', 
                b: 'list[N],N>0') -> 'list[N]':
    pass    
    
def my_function(a, b):
    """ Function description.
    :param a: some description
    :type a: int,>0
    
    :param b: some descrtipn for b
    :tybe b: list[N],N>0
    :rtype: list[N]
    """
    pass
    
contracts.disable_all()
DISABLE_CONTRACTS
```

## Data classes - TypeDict
```python
class BookingResult(TypeDict):
    user_id = f.PositiveInt()
    order_id = f.PositiveInt()
    created_at = f.DateTime()
    price = f.Demical()
    comment = opt(f.StringNoEmpty)
    

class BookingRequest(TypeDict):

    ROOM_TYPES = ('Economy', 'Standart', 'Premium')
    
    hotel_id = f.PositiveInt()
    checkin_date = f.Date()
    checkout_date = f.Date()
    room_type = f.StringNoEmpty(choices=ROOM_TYPES)
    
@contract
def make_booking(request: BookingRequest) -> BookingResult:
    # ...
    pass
    return BookingResult(
        user_id=1,
        order_id=10,
        created_at=datetime.now(),
        price=Decimal('10000'),
        comment='hello!'
    ) 
    
# example of test
book_request = BookingRequest(
    hotel_id=10,
    checkin_date=datetime.date(2016, 1, 2),
    checkout_date=datetime.date(2016, 1, 10)
)
response = self.post('/api/book/', data=book_request.simplify())
self.assertEqual(response.status, 200)

# validate correct response format
book_result = BookingResult(**response.json())
self.assertEqual(book_result.order_id, 10)

# instead typedict - use https://github.com/schematics/schematics
# python3 type hinting

```
Object - cам объект (результат после инициализации класса)

Class - програмный код, описывающий объект

Object Class - объект описывающий Class


Интерфейс - совокупность всех способов взаимодействия с объектом(то что будет видеть пользователь, публичные)
Реализация - внутренняя часть класса(совокупность всех приватных, переменных, открытых)
Приватные методы и переменные на самом деле не относятся к интерфейсу.

Наследование  - родитель-потомок
Полиморфизм - один и тот же код для работы с различными объектами, которые имеют одинаковый интерфейс
Парадигма полиморфизма позволяет вместо объекта базового типа использовать его потомка, при этом не указывая это явно.
 
Инкапсуляция - Парадигма инкапсуляции предлагает объединять переменные и методы, относящиеся к одному объекту в единый компонент. По сути соблюдение парадигмы инкапсуляции и заключается в создании классов.


### SOLID
- problems:
- unflexible systems
- too much links between components
- unless functions


Single responsibility - единственная ответственность. Один объект - одна ответственность.
Open/Closed - открыты для расширения, закрыты для изменения
Liskov substitution - функции должны иметь возможность использовать его подтипы не зная об этом. ?
Interface segregation - Не следует делать сложный интерфейс. К каждой части - единый интерфейс.
Dependency inversion - Модули верхних уровней не зависят от нижних. Оба типа - зависят от абстракций.
    Абстракции не должны зависеть от деталей
    Детали зависят от абстракций
    http://take.ms/8axCFd
    
Приведем пример. Пусть у вас есть базовый класс Distributer, который может отправлять сообщения в различные социальные сети. 
У этого класса есть несколько реализаций, например VKDistributer и OKDistributer. 
Согласно принципу инверсии зависимостей, эти реализации не должны зависеть от методов класса Distributer 
    (например VK_send_message и OK_send_message). 
Вместо этого у класса Destributor должен быть объявлен общий абстрактный метод send_message, который и будет реализован отдельно в каждом из потомков.

# private variable
```python
class Foo:
    def __init__(self):
        self.__privateField = 4

foo = Foo()
foo._Foo__privateField
```
   

Абстрактные методы
```python
from abc import ABC, abstractmethod

class A(ABC):
    @abstractmethod
    def do_something(self):
        print("Hi") 
        
a = A()  # will raise exception, because do_something is not realised


class B(A):
    def do_somethig_else(self):
        print('Hi2')
b = B() #- will also raise exception

class B(A):
    def do_somethig_else(self):
        print('Hi2')
    
    def do_somethig(self):
        print('Hi2')
        
#now all is ok
s

```

# UML - унифицированный язык моделирования
### Типы отношений
```
->    Ассоциация (работник и задача) (
-|>   Наследование (животные и млекопетающие)
- - |>Реализация, имплементация, реализация абстрактного класса в настоящем.
- - > Зависимость (изменение спецификации одного - приводит к изменению другого, экземпляр одного класса - входит как переменная в другой класс)
-<>   Агрегация (класс коллекции и его список)
-<>(закр)Композиция (при уничтожении контейнера будет уничтожено содержимое)
http://take.ms/A7h3V
```


# Рефакторинг программ
Процесс преобразования для облегчения его понимания
Оптимизация - увеличение производтельности
реинжениринг - внутренняя структура и работа меняется

"Запахи кода"
- дублирование кода
- длинные методы 
- слишком большие классы (делить на более мелкие)
- длинный список параметров -> выделение параметров в объект
- "жадные" функции -> когда методы одного класса часто вызывают другой класс
- избыточные временные переменные -> 
- классы данных (!?) -> Только поля и нет методов - возможно нужен словарь
- несгруппированные данные -> объединение в класс


# Паттерны
Повторяемая архитектурная конструкция, применяемая для решения частых задач

## Низкоуровневые паттерны(идиомы)
```python
result = true_val if condition else false_val
result = [false_val, true_val][condition]
new_list = [f(x) for x in iterable if condition]
```

## Паттерны проектирования
###Структурные
Модифицируют структуру объекта, для получения из класса более сложных структур или альтернативный доступ к объекту
- Адаптер (Adapter) – взаимодействие несовместимых объектов
- Мост (Bridge) – разделение абстракции и реализации
- Компоновщик (Composite) – агрегирование нескольких объектов в одну структуру
- Декоратор (Decorator) – динамическое создание дополнительного поведения объекта
- Фасад (Facade) – сокрытие сложной структуры за одним объектом, являющимся общей точкой доступа
- Приспособленец (Flyweight) – общий объект, имеющий различные свойства в разных местах программы
- Заместитель (Proxy) – контроль доступа к некоторому объекту

###Порождающие
Используются при создании различных объектов. Разделить создание и использование.
- Абстрактная фабрика (Abstract factory) — создание семейств взаимосвязанных объектов
- Строитель (Builder) — сокрытие инициализации для сложного объекта
- Фабричный метод (Fаctory method) — общий интерфейс создания экзкмпляров подклассов некоторого класса
- Отложенная инициализация (Lazy initialization) — создание объекта только при доступе к нему
- Пул одиночек/Объектный пул (Multiton/Object pool) — повторное использование сложных объектов вместо повторного создания
- Прототип (Prototype) — упрощение создания объекта за счет клонирования уже имеющегося
- Одиночка (Singleton) — объект, присутствующий в системе в единственоом экземпляре

###Поведенческие
Описывают способы взаимодействия объектов различных типов
- Цепочка обязанностей (Chain of Responsibility) — обработка данных несколькими объектами
- Интерпретатор (Interpreter) — решение частой незначительно изменяющейся задачи
- Итератор (Iterator) — последовательный доступ к объекту-коллекции
- Хранитель (Memento) — сохранение и восстановление объекта
- Наблюдатель (Observer) — оповещение об изменении некоторого объекта
- Состояние (State) — изменение поведения в зависимости от состояния
- Стратегия (Strategy) — выбор из нескольких вариантов поведения объекта
- Посетитель (Visitor) — выполнение некоторой операции над группой различных объектов


###Конкурентные
Особые шаблоны для паралельного программирования, для взаимодействия процессов и потоков
Блокировка - позволяет потоку захватывать общие ресурсы на период выполнения
Монитор - механизм синхронизации и взаимодействия процессов, обеспечивающий доступ к общим неразделяемым ресурсам
Планировщик - Позволяет планировать порядок выполнения паралельных процессов с учетом приоритетов и ограничений
активный объект - позволяет отделять поток выполнения некоторого метода от потока в котором метод был вызван

## Архитектурные паттерны
Model-view-controller
model-view-presenter
etc







