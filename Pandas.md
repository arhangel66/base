pivot\_table

```python
fc = df.pivot_table(index=['facility_name'], 
                    columns=['menu__date', 'menu__meal__name_short'],
                    values=['quantity'], fill_value=0, aggfunc=np.sum, margins=True).astype(int)

df.nunique()  # unique count
df.count()  # count
df.astype(str)

df.columns.values  # show columns

```

save

```python

```

