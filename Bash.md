## basic
```bash
ls -la   # show all in current folder
ls -lct  # show date of changes
rm -rf directoryname  # delete folder with all in it

cp -R ~/sites/mikhail/current/settings_local.py ~/sites/mikhailssl/current/settings_local.py
cp -R ~/sites/mikhailssl/conf/settings_local.py ~/sites/mikhailssl/current/settings_local.py  # copy
cp -R t1/. t2/   # copy all files

# copy folder from ssh
scp -r -i ~/.ssh/GTG_05032016.pem 52.6.72.82:/home/django/projects/gtg_api /Users/mikhail/w/gtg_api/

mv settings_local.py settings_local_old.py  # rename file

sudo su # login as root
cat file.txt # show file content on screen
```

```bash
cd ~/../../Applications/
open Upwork.app --args --enable-gpu

pkill -f tranquil/bin  # will kill all process by part of name
pkill -f mikhailssl
pkill -f firefox

find . -name "*.pyc" -exec rm -f {} \;  # kill all pyc

apt-get install htop
ps auxww|grep mysql

ps aux | grep do2.py

lsof -c mysql | wc -l

lsof -c chromium | wc -l
lsof -c Xvfb - запущенные процессы
lsof /var/www/live/ | wc -l - количество открытых файлов
lsof +D /var/log | wc -l
lsof -c firefox | wc -l
lsof -c chrome | wc -l
lsof -p 32328 | wc -l
```

### symlink
```bash
ls -la  | grep "\->"  # show link of symlinks
ln -s source_file myfile  # create symbolinc link
ln -sfn /a/new/path files  # change symbolic link
ln -s ~/w/base/projects/schule_notes.md schule_notes.md
ln -s ~/w/base/projects/notes.md mongo_notes.md
ln -s ~/w/base/projects/masstuber_notes.md mt_notes.md
ln -s ~/w/base/projects/profileyou_notes.md profileyou_notes.md
ln -s ~/w/base/projects/vq_notes.md ~/w/vq/video-queue/vq_notes.md
ln -s ~/w/base/projects/irs_notes.md ~/w/irs-eindocs/irs_notes.md
ln -s ~/w/base/projects/monitorscripts_notes.md ~/w/vq/video-queue/monitorscripts_notes.md
ln -s ~/w/base/projects/resume_notes.md ~/w/resume_b/resume_notes.md
ln -s ~/w/base/projects/cancel_notes.md ~/w/newcansel/cancellation/notes.md
ln -s ~/w/base/instructions/host_new_domain_rovin.md ~/w/newcansel/cancellation/deploy.md



# copy file from ssh
scp -p 40022 smplan@demo.smplan.ovh:~/sites/mikhailssl/emnifyAPIToken.txt


# nano
ctrl + _ # jump to line
alt + c # show line number
nano +246 file.log # show  from line 247

# TODO quick search in log file
```

