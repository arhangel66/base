### work woth time
```python
dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
datetime.timedelta(days=0, seconds=0, microseconds=0, milliseconds=0, minutes=0, hours=0, weeks=0)
datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
datetime.datetime(year, month, day, hour, minute, second=0)
strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())

datetime.today().year, datetime.today().month
#'Thu, 28 Jun 2001 14:17:15 +0000'
#11 Nov 2016 13:21

# from text to time
datetime.strptime("05 Jun 2016 08:35:43", "%d %b %Y %H:%M:%S")
datetime.strptime("2017-12-16", "%Y-%m-%d")

# group list of dict    
res = {}
for item in lFacilityData:
    res.setdefault(item[sGroupKey], []).append(item)
    
# filter list
list(filter(re.compile("tax .*").match, lTest))

```



### send emails for pure python3
```python
fromaddr = 'arhangel66@yandex.ru'
toaddrs = 'arhangel662@gmail.com'
msg = MIMEMultipart('')
msg['Subject'] = 'Report'
msg['From'] = 'arhangel66@yandex.ru'
msg['To'] = 'arhangel662@gmail.com'

# attach pdf
with open("report.pdf", "rb") as opened:
    file_content = opened.read()
attachedfile = MIMEApplication(file_content, _subtype="pdf", _encoder=encode_base64)
attachedfile.add_header('Content-Disposition', 'attachment', filename='report.pdf')
msg.attach(attachedfile)

# Credentials (if needed)
username = 'arhangel66'
password = ''
server = smtplib.SMTP_SSL('smtp.yandex.ru:465')
server.login(username, password)
server.send_message(msg)
server.quit()

# logging
# https://docs.djangoproject.com/en/2.0/topics/logging/#configuring-logging # TODO learn
from logging import getLogger
logger = getLogger(__name__)
logging.basicConfig(
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.StreamHandler()
    ],
    level=logging.INFO
)
logger = getLogger(__name__)
traceback.print_exc()

# show as a table
from tabulate import tabulate
print(tabulate([['Alice', 24], ['Bob', 19]], headers=['Name', 'Age']))
#Name      Age
#------  -----
#Alice      24
#Bob        19
```

```bash
workon
mkvirtualenv -p ~/anaconda2/bin/python2 masstuber
mkvirtualenv -p /Library/Frameworks/Python.framework/Versions/3.6/bin/python3 data-cleaning4 -  will create with python3.6 
mkvirtualenv flaskask  - will create new virtualenvwrapper
setvirtualenvproject - will set current dir as project root
rmvirtualenv ENVNAME
```


# python strings
```python
name = 'bob'

# old style
"Hello, %s" % name

# new style
"Hello, {}".format(name)

# for python 3.6
name = 'Jon'
dName = {'first': 'Jon', 'last': 'Brut'}
f"Hello, {name}"
print(f"Hello, {dName['first']}")

```

# property
```python
class Celsius:
    def __init__(self, temperature = 0):
        self.temperature = temperature

    def to_fahrenheit(self):
        return (self.temperature * 1.8) + 32

    def get_temperature(self):
        print("Getting value")
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value

    temperature = property(get_temperature,set_temperature)
    
c = Celsius()
c.temperature
#Getting value
#0

c.temperature = 37
# set value
# 37
```