```sh
celery -A kg worker -l info
-A kg # means kg application
-l info # means info log

celery -A kg purge  # stop all current tasks

```